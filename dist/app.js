"use strict";
const startButton = document.querySelector('#startBtn');
const pauseButton = document.querySelector('#pauseBtn');
const stopButton = document.querySelector('#stopBtn');
const showTimer = document.querySelector('#timer');
let hour = 0;
let min = 0;
let sec = 0;
let timerLive = false;
const formatTime = function (hour, min, sec) {
    const hours = `${hour}`.padStart(2, '0');
    const minutes = `${min}`.padStart(2, '0');
    const seconds = `${sec}`.padStart(2, '0');
    return `${hours}:${minutes}:${seconds}`;
};
const checkTime = function () {
    if (timerLive) {
        if (sec === 59) {
            min += 1;
            sec = 0;
        }
        if (min === 59) {
            hour += 1;
            min = 0;
        }
        if (hour === 24) {
            hour = 0;
            min = 0;
            sec = 0;
        }
        sec++;
        showTimer.innerHTML = formatTime(hour, min, sec);
    }
};
const callTimer = setInterval(checkTime, 1000);
startButton.addEventListener('click', () => {
    timerLive = true;
    callTimer;
});
pauseButton === null || pauseButton === void 0 ? void 0 : pauseButton.addEventListener('click', () => (timerLive = false));
stopButton === null || stopButton === void 0 ? void 0 : stopButton.addEventListener('click', () => {
    showTimer.innerHTML = '00:00:00';
    timerLive = false;
    sec = 0;
    min = 0;
    hour = 0;
});
