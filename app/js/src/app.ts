const startButton: any = document.querySelector('#startBtn');
const pauseButton = document.querySelector('#pauseBtn');
const stopButton = document.querySelector('#stopBtn');

const showTimer: any = document.querySelector('#timer');

// time variables
let hour: number = 0;
let min: number = 0;
let sec: number = 0;
let timerLive = false;

// formatting time
const formatTime = function (hour: number, min: number, sec: number): string {
  const hours: string = `${hour}`.padStart(2, '0');
  const minutes: string = `${min}`.padStart(2, '0');
  const seconds: string = `${sec}`.padStart(2, '0');

  return `${hours}:${minutes}:${seconds}`;
};

// checking if timer is running or not
const checkTime = function (): any {
  if (timerLive) {
    if (sec === 59) {
      min += 1;
      sec = 0;
    }
    if (min === 59) {
      hour += 1;
      min = 0;
    }
    if (hour === 24) {
      hour = 0;
      min = 0;
      sec = 0;
    }
    sec++;
    showTimer.innerHTML = formatTime(hour, min, sec);
  }
};
// interval for timer
const callTimer: number = setInterval(checkTime, 1000);

// start timer
startButton.addEventListener('click', () => {
  timerLive = true;
  callTimer;
});

// pause timer
pauseButton?.addEventListener('click', () => (timerLive = false));

// stop timer
stopButton?.addEventListener('click', () => {
  showTimer.innerHTML = '00:00:00';
  timerLive = false;
  sec = 0;
  min = 0;
  hour = 0;
});
